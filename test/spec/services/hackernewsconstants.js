'use strict';

describe('Service: HackerNewsConstants', function () {

  // load the service's module
  beforeEach(module('hackerNewsApp'));

  // instantiate service
  var HackerNewsConstants;
  beforeEach(inject(function (_HackerNewsConstants_) {
    HackerNewsConstants = _HackerNewsConstants_;
  }));

  it('should do something', function () {
    expect(!!HackerNewsConstants).toBe(true);
  });

});
