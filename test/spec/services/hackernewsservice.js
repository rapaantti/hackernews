'use strict';

describe('Service: HackerNewsService', function () {

  // load the service's module
  beforeEach(module('hackerNewsApp'));

  // instantiate service
  var HackerNewsService;
  beforeEach(inject(function (_HackerNewsService_) {
    HackerNewsService = _HackerNewsService_;
  }));

  it('should do something', function () {
    expect(!!HackerNewsService).toBe(true);
  });

});
