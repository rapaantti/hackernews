'use strict';

describe('Directive: hackerNewsPostsDirective', function () {

  // load the directive's module
  beforeEach(module('hackerNewsApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<hacker-news-posts-directive></hacker-news-posts-directive>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the hackerNewsPostsDirective directive');
  }));
});
