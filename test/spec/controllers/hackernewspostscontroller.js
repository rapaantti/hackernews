'use strict';

describe('Controller: HackernewspostscontrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('hackerNewsApp'));

  var HackernewspostscontrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HackernewspostscontrollerCtrl = $controller('HackernewspostscontrollerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(HackernewspostscontrollerCtrl.awesomeThings.length).toBe(3);
  });
});
