# HackerNews

Simple demonstration that duplicates some features from site [http://www.hntoplinks.com/](http://www.hntoplinks.com/). See live demo at [http://46.101.196.196/](http://46.101.196.196/)

## Build & development
Run `sudo apt update` for update repositories

Run `sudo apt install npm` for install npm

Run `npm install -g bower` for install bower

Run `npm install` for install node modules

Run `bower install` for install bower components

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma. **Tests are not implemented in this demo.**