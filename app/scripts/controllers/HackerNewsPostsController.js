'use strict';

/**
 * @ngdoc controller
 * @name  hackerNewsApp.controller:HackerNewsPostsController
 * @description
 * # HackerNewsPostsController
 * Controller for HackerNewsPostsDirective
 */
angular.module('hackerNewsApp')
  .controller('HackerNewsPostsController', function ($q, HackerNewsService, ngProgressFactory) {
    var vm = this;

    // Local variables
    vm.chunks = [];
    vm.stories = [];
    vm.loading = true;
    vm.loaded = 0;
    vm.progressBar = ngProgressFactory.createInstance();

    /**
     * Initialize function for controller.
     */
    function init() {
      HackerNewsService.getTopStories()
          .then(_onSuccess);

      /**
       * Handles success callbacks
       *
       * @param {*}    result
       *
       * @private
       */
      function _onSuccess(result) {
        var storyIds = result.data || [];

        while (storyIds.length) {
          vm.chunks .push(storyIds.splice(0, 5));
        }

        vm.progressBar.start();
        fetchChunk();
      }
    }

    /**
     * Method to fetch given chunk from HackerNews APi
     *
     * @param {Number}   chunkNumber
     */
    function fetchChunk(chunkNumber) {
      chunkNumber = chunkNumber || 0;
      vm.progressBar.set((chunkNumber / vm.chunks.length * 100));

      if (vm.chunks.length === chunkNumber) {
        vm.progressBar.complete();
        return;
      }

      var storyIds = vm.chunks[chunkNumber];
      var promises = [];

      angular.forEach(storyIds, function forEach(storyId) {
        var promise = HackerNewsService.getStory(storyId);
        promises.push(promise);
      });

      $q.all(promises).then(_onSuccess);

      /**
       * Handles success callbacks
       *
       * @param {*}    result
       *
       * @private
       */
      function _onSuccess(result) {
        angular.forEach(result, function(story) {
          vm.stories.push(story.data);
        });

        fetchChunk(++chunkNumber);
      }
    }

    init();
  });
