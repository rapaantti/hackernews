'use strict';

/**
 * @ngdoc directive
 * @name  hackerNewsApp.directive:hackerNewsPostsDirective
 * @description
 * # hackerNewsPostsDirective
 * Directive for posts which can be filtered using query
 */
angular.module('hackerNewsApp')
  .directive('hackerNewsPostsDirective', function () {
    return {
      templateUrl: 'views/stories.html',
      restrict: 'E',
      controller: 'HackerNewsPostsController',
      scope: {
        query: '='
      },
      controllerAs: 'vm',
      bindToController: true
    };
  })
;
