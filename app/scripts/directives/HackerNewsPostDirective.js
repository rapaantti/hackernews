'use strict';

/**
 * @ngdoc directive
 * @name  hackerNewsApp.directive:hackerNewsPostDirective
 * @description
 * # hackerNewsPostDirective
 * Directive for one post
 */
angular.module('hackerNewsApp')
  .directive('hackerNewsPostDirective', function () {
    return {
      templateUrl: 'views/story.html',
      restrict: 'E',
      scope: {
        story: '='
      }
    };
  });
