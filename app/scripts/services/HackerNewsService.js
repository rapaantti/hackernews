'use strict';

/**
 * @ngdoc service
 * @name  hackerNewsApp.HackerNewsService
 * @description
 * # HackerNewsService
 * Simple service for fetching data from hacker news API.
 */
angular.module('hackerNewsApp')
  .service('HackerNewsService', function ($http, HackerNewsConstants) {
    return {
      getTopStories: getTopStories,
      getStory: getStory
    };

    /**
     * Returns top stories from hacker news.
     *
     * @returns {HttpPromise}
     */
    function getTopStories() {
      return $http.get(HackerNewsConstants.BASE_URL + 'topstories.json?print=pretty')
    }

    /**
     * Returns data for a story.
     *
     * @param {number}    storyId
     *
     * @returns {HttpPromise}
     */
    function getStory(storyId) {
      return $http.get(HackerNewsConstants.BASE_URL + 'item/' + storyId + '.json?print=pretty');
    }
  });
