'use strict';

/**
 * @ngdoc service
 * @name  hackerNewsApp.HackerNewsConstants
 * @description
 * # HackerNewsConstants
 * Constant in the hackerNewsApp.
 */
angular.module('hackerNewsApp')
  .constant('HackerNewsConstants', {
    BASE_URL: 'https://hacker-news.firebaseio.com/v0/'
  })
;
